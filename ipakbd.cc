#include <stdio.h>
#include <gtkmm.h>
const char // copied from the Wikipedia IPA chart, minus Latin chars
	*nasal[] = {"m̥","ɱ","n̪","n̥","n̠","ɳ","ɲ̟", "ɳ̊","ɲ̊","ŋ̊","ŋ","ɴ"},
	*stop[] = {"p̪","b̪","t̪","d̪","ʈ","ɖ","ɟ","ɢ","ʡ","ʔ"},
	*sibilant[] = {"s̪","z̪","ʃ","ʒ","ʂ","ʐ","ɕ","ʑ"},
	*fricative[] = {"ɸ","β","θ","ð","θ̱","ð̠","ç","ʝ","ɣ","χ","ʁ","ħ","ʕ","ʜ","ʢ","ɦ"},
	*approximant[] = {"ʋ","ɹ","ɻ","j̊","ɰ"},
	*flap[] = {"ⱱ̟","ⱱ","ɾ","ɽ","ɢ̆","ʡ̯"},
	*trill[] = {"ʙ","r̥","r","ɽ͡r","ʀ"},
	*latfric[]={"ɬ","ɮ","ꞎ","ʎ̝̊","ʟ̝̊","ʟ̝"},
	*latapprox[]={"l̥","ɭ","ʎ̟","ʎ","ʟ"},
	*latflap[]={"ɺ","ɺ̢","ʎ̯","ʟ̆"},
	*clicks[]={"ʘ","ǀ","ǃ","ǂ","ǁ","ʘ̃","ʘ̃ˀ","ʘ͡q","ʘ͡qʼ","‼"},
	*diacritics[]={"ʰ","ʱ","ˡ","̚","ⁿ","͡","ʷ","ˠ","ᶣ","ʲ","ˤ","˞","ʼ","̊","̥", "̩", "̍", "̃"},
	*suprasegmentals[]={"ˈ","ˌ","ː","ˑ","‿"},
	*vowels1[] = {"ɨ","ʉ","ɯ","ɪ","ʏ","ɪ̈","ʊ̈","ʊ"},
	*vowels2[] = {"ø","ɘ","ɵ","ɤ","e̞","ø̞","ə","ɤ̞","o̞"},
	*vowels3[] = {"ɛ","œ","ɜ","ɞ","ʌ","ɔ","æ","ɐ","ɶ","ä","ɑ","ɒ"},
	*implosives[] = {"ɓ̥","ɓ","ɗ̥","ɗ","ᶑ","ʄ̊","ʄ","ɠ","ʛ"},
	*toneletters[] = {"˥","˦","˧","˨","˩"};

#define ccount(A) (sizeof(A) / sizeof(const char*))
struct IPAgroup {
	const char* manner;
	const char** members;
	const size_t memberc;
} cons1[] = {
	{"Nasal",				nasal,		ccount(nasal)},
	{"Stop",				stop,		ccount(stop)},
	{"Fricative",			sibilant,	ccount(sibilant)},
	{0,						fricative,	ccount(fricative)},
	{"Approximant",			approximant,ccount(approximant)},
	{"Flap or tap",			flap,		ccount(flap)},
}, cons2[] = {
	{"Trill",				trill,		ccount(trill)},
	{"Lateral fricative",	latfric,	ccount(latfric)},
	{"Lateral approximant",	latapprox,	ccount(latapprox)},
	{"Lateral flap",		latflap,	ccount(latflap)},
	{"Clicks",				clicks,		ccount(clicks)},
	{"Implosives",			implosives,	ccount(implosives)},
}, diacvowels[] = {
	{"Suprasegmentals",	suprasegmentals,	ccount(suprasegmentals)},
	{"Diacritics",	diacritics,	ccount(diacritics)},
	{"Tone letters",toneletters,	ccount(toneletters)},
	{0,				vowels1,	ccount(vowels1)},
	{0,				vowels2,	ccount(vowels2)},
	{0,				vowels3,	ccount(vowels3)},
};
#undef ccount

#define ccount(A) (sizeof(A) / sizeof(const IPAgroup))
struct IPAcategory {
	const char* heading;
	const IPAgroup* members;
	const size_t memberc;
} IPAcategories[] = {
	{"Consonants 1", cons1, ccount(cons1)},
	{"Consonants 2", cons2, ccount(cons2)},
	{"Diacritics and Vowels", diacvowels, ccount(diacvowels)},
};
const size_t IPAcategoryc = sizeof(IPAcategories)/sizeof(IPAcategory);

#undef ccount



Gtk::Entry* textptr;
Gtk::Window* windowptr;
// this is so stupid, but if we try initialize things outside main GTK throws a tantrum

void onclick(const char* val) {
	textptr->set_text(textptr->get_text()+val);
	windowptr->set_focus(*textptr);
	textptr->set_position(textptr->get_text_length());
}

int main (int argc, char** argv) {
	Gtk::Main kit(argc, argv);
	Gtk::Window window; windowptr=&window;
	Gtk::VBox controls;
	Gtk::HBox chars;
	Gtk::Entry text; textptr=&text;
	window.set_title("IPA Keyboard");
	//window.set_border_width(5);
	chars.set_spacing(7);
	chars.set_border_width(5);
	for (size_t c = 0; c<IPAcategoryc; c++) {
		Gtk::VBox* table = new Gtk::VBox;
		for (size_t g = 0; g<IPAcategories[c].memberc; g++) {
			Gtk::HBox* b = new Gtk::HBox();
			if (IPAcategories[c].members[g].manner!=0) {
				Gtk::Label* l = new Gtk::Label(IPAcategories[c].members[g].manner);
				l->show();
				b->add(*l);
			}
			for (size_t i = 0; i<IPAcategories[c].members[g].memberc; i++) {
				Gtk::Button* btn = new Gtk::Button(IPAcategories[c].members[g].members[i]);
				btn->signal_clicked().connect(
					sigc::bind<const char*>(sigc::ptr_fun(&onclick), IPAcategories[c].members[g].members[i])
				);	// FUNCTIONAL PROGRAMMING IS STUPID WITHOUT COMPILER SUPPORT,
					// I'M JUST SAYIN'
				btn->show();
				b->add(*btn);
			}
			b->show();
			table->add(*b);
		}
		table->show();
		chars.add(*table);
	}
	chars.show();
	controls.add(chars);
	text.show();
	controls.add(text);
	controls.show();
	window.add(controls);
	Gtk::Main::run(window);
	return 0;
}
