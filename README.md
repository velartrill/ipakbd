ipakbd
======

A butt-ugly onscreen IPA keyboard for Linux. Known to compile and run on Arch Linux.

## Requirements
* g++
* gtkmm-2.4

### Compilation
Run `./make.sh` to compile.

#### Compiling under clang
ipakbd will compile under clang, but the -lstdc++ flag must be added to the compiler invocation in `make.sh`.
