#!/usr/bin/bash
clang++ -std=c++11 \
	ipakbd.cc	\
	`pkg-config gtkmm-2.4 --cflags --libs` \
	-o ipakbd \
	-O3
echo "Compiled to ipakbd"
